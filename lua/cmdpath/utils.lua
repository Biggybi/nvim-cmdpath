local options = require("cmdpath.config").options
local M = {}

---@param path string
function M.tilde_path(path)
	local tilde_path = string.gsub(path, "^" .. os.getenv("HOME"), "~")
	if tilde_path ~= "/" and tilde_path[-1] ~= "/" then
		return tilde_path .. "/"
	end
	return tilde_path
end

---@param path string
function M.path_tilde_opt(path)
	if options.path.tilde == true then
		return M.tilde_path(path)
	end
	return path
end

function M.relative_path()
	return vim.fn.expand("%:h") .. "/"
end

function M.file_path()
	return vim.fn.expand("%:p:h")
end

function M.term_path()
	-- NOTE: only works after cd for enabled `:h terminal-osc7`
	return vim.fn.system("pwd"):gsub("\n", "")
end

function M.get_path()
	if vim.o.buftype == "terminal" then
		return M.path_tilde_opt(M.term_path())
	end
	if options.path.relative == true then
		return M.relative_path()
	end
	return M.path_tilde_opt(M.file_path())
end

return M
