local M = {}

---@class command_man
---@field command string
---@field key string

---@class commands
---@field vertical_split command_man
---@field horizontal_split command_man
---@field tab command_man
---@field window command_man

---@class path_options
---@field tilde boolean
---@field relative boolean

---@class command
---@field cmd string
---@field key string
---@field desc string

---@class user_options
---@field path path_options
---@field map_leader string
---@field commands command[]
---@field ctrl boolean

---@class config
---@field options user_options

---@type user_options
local defaults = {
	path = {
		relative = false, -- use relative path to `cwd`
		tilde = true, -- use `~` for $HOME (only makes sense with `relative = false`)
	},
	map_leader = "<c-n>", -- prefix for the extension
	commands = {
		vertical_split = { cmd = "vs", key = "v", desc = "CmdPath vertcal" },
		horizontal_split = { cmd = "sp", key = "s", desc = "CmdPath horizontal" },
		tab = { cmd = "tabnew", key = "t", desc = "CmdPath tab" },
		window = { cmd = "e", key = "n", desc = "CmdPath" },
	},
	ctrl = true,
}

function M.set(user_options)
	M.options = vim.tbl_extend("force", defaults, user_options or {})
end

M.options = defaults

return M
