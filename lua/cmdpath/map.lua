local M = {}
local options = require("cmdpath.config").options
local utils = require("cmdpath.utils")

local map = vim.keymap.set

function M.map()
	local leader = options.map_leader
	for _, command in pairs(options.commands) do
		local function rhs()
			return ":" .. command.cmd .. " " .. utils.get_path()
		end
		map("n", leader .. command.key, rhs, { expr = true, desc = command.desc })
		map("n", leader .. "<c-" .. command.key .. ">", rhs, { expr = true, desc = command.desc })
	end
end

M.cmd_name = "CmdPath"

return M
