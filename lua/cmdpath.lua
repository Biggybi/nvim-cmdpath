local M = {}
local config = require("cmdpath.config")
local map = require("cmdpath.map")

function M.setup(user_options)
	config.set(user_options)
	map.map()
	vim.g.loaded_nvim_cmdpath = 1
end

return M
